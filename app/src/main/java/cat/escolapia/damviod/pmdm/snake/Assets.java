package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap Wall;
    public static Pixmap background2;
    public static Pixmap background3;
    public static Pixmap CurrentBackground;
    public static Pixmap logo;
    public static Pixmap newMainMenu;
    public static Pixmap NewLogo;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap Diamond2;
    public static Pixmap Credits;
    public static Sound click;
    public static Sound eat;
    public static Sound xoc;
    public static Music intro;
    public static Music gamemusic;
    public static Music gameover;
}
