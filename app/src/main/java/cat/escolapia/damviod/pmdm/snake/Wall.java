package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Wall {
    public List<WallPart> wallParts = new ArrayList<WallPart>();

    public Wall() {
        wallParts.add(new WallPart(8, 6));
        wallParts.add(new WallPart(8, 7));
        wallParts.add(new WallPart(8, 8));
    }

}