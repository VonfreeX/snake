package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.15f;
    boolean speed=false;
    public Wall wall;
    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;

    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    boolean fields2[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        wall=new Wall();
        snake = new Snake();
        diamond=placeDiamond();
        diamond2=placeDiamond();


    }

    private Diamond placeDiamond() {
        Diamond d;
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields2[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }
        int walllenght = wall.wallParts.size();
        for (int i = 0; i < walllenght; i++) {
            WallPart wp = wall.wallParts.get(i);
            fields2[wp.x][wp.y] = true;
        }
        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false&&fields2[diamondX][diamondY] == false) break;

            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        d=new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        return d;

    }

    public void update(float deltaTime) {
        if (gameOver) return;
        Assets.intro.stop();
        Assets.gamemusic.play();
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()||snake.checkXocaWall()) {
                gameOver = true;
                return;
            }
            if (snake.CheckXocaDiamant(diamond)) {
                snake.allarga();
                diamond=placeDiamond();
                score=score+SCORE_INCREMENT;
            }
            if (snake.CheckXocaDiamant(diamond2)) {
                snake.allarga();
                diamond2=placeDiamond();
                score=score+SCORE_INCREMENT;
            }

            CheckScore();
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }
    public String getScore()
    {
        int score2=score;

        return Integer.toString(score2);
    }
    public void CheckScore()
    {
        if (score==30)
        {
            if (speed==false) {
                tick = tick - TICK_DECREMENT;
                speed=true;

            }
            Assets.CurrentBackground=Assets.background2;
        }
        if  (score==100)
        {
            if (speed==true) {

                tick = tick - TICK_DECREMENT;
                speed=false;
            }
            Assets.CurrentBackground=Assets.background3;
        }
    }
}
