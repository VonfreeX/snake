package cat.escolapia.damviod.pmdm.snake;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class MainMenuScreen extends Screen {
    public MainMenuScreen(Game game) {
        super(game);

    }   

    public void update(float deltaTime) {
        Assets.intro.play();
        if (Assets.gamemusic.isPlaying())
        {
            Assets.gamemusic.stop();
        }
        if (Assets.gameover.isPlaying())
        {
            Assets.gameover.stop();
        }

        Graphics g = game.getGraphics();
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();       

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {

                if(inBounds(event, 64, 220, 100, 36) ) {
                    game.setScreen(new GameScreen(game));
                    Assets.click.play(1);
                    return;
                }
                if(inBounds(event, 64, 220 + 40, 180, 36) ) {
                    game.setScreen(new HighscoreScreen(game));
                    Assets.click.play(1);
                    return;
                }
                if(inBounds(event, 64, 220 + 80, 180, 36) ) {
                    game.setScreen(new Credits(game));
                    Assets.click.play(1);
                    return;
                }

            }
        }
    }
    
    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 && 
           event.y > y && event.y < y + height - 1) 
            return true;
        else
            return false;
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.NewLogo, 32, 20);
        g.drawPixmap(Assets.newMainMenu, 64, 220);
    }

    public void pause() {        
        Settings.save(game.getFileIO());
        Assets.intro.stop();
    }

    public void resume() {

    }

    public void dispose() {

    }
}

