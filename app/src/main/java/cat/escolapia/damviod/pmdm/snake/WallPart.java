package cat.escolapia.damviod.pmdm.snake;

public class WallPart {
    public int x, y;
    
    public WallPart(int x, int y) {
        this.x = x;
        this.y = y;
    }
}       
